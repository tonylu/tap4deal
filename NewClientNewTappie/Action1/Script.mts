﻿'Script Name: New client with new Tappie card activate
'Title: Check a new client can activate a new tappie card with a new email address
'URL: http://tap4deal.a2a-tech.com/index.php?route=account/activate
'IE 10.0.4 (KB2817183)
'
'Author: Tony Lu, <xudong.lu@yahoo.com>
'Change log: 
'      ---  Created by Tony Lu on 2013.05.15
'QA Manager: Alvin
'=======================================================================================

SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe"
wait(2)
Browser("MainBrowser").Navigate "http://tap4deal.a2a-tech.com/"
Browser("MainBrowser").Page("HomePage").Sync
Browser("MainBrowser").Page("HomePage").Link("Sign In").Click
Browser("MainBrowser").Page("Sign in").Link("hereToActivate").Click
Browser("MainBrowser").Page("TappieCardActivate").WebEdit("card_number").Set RandomNumber("NewTappieNumber")
Browser("MainBrowser").Page("TappieCardActivate").WebEdit("email").Set Parameter("newEmail")
Browser("MainBrowser").Page("TappieCardActivate").WebEdit("password").Set Parameter("password")
Browser("MainBrowser").Page("TappieCardActivate").WebEdit("confirmPassword").Set Parameter("password")
Browser("MainBrowser").Page("TappieCardActivate").WebCheckBox("agreeCheckBox").Set "on"
Browser("MainBrowser").Page("TappieCardActivate").Link("SignUpWithNewTappie").Click
If Browser("MainBrowser").Page("TappieCardActivate").WebElement("CongratulationsToNewCountCreated").Exist(2) Then
	Reporter.ReportEvent micPass, "Check new client can create new count", "New count created successfully"
ElseIf Not Browser("MainBrowser").Page("TappieCardActivate").WebElement("CongratulationsToNewCountCreated").Exist(2) Then
	Reporter.ReportEvent micFail, "Check new client can create new count", "New count create failed"
End If
