﻿'Script Name: client reset password
'Title: Check an existing client can reset password via a registered email
'URL: http://tap4deal.a2a-tech.com/index.php?route=account/forgotten
'IE 10.0.4 (KB2817183)
'
'Author: Tony Lu, <xudong.lu@yahoo.com>
'Change log: 
'      ---  Created by Tony Lu on 2013.05.15
'QA Manager: Alvin
'=======================================================================================

SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe"
wait(2)
Browser("MainBrowser").Navigate "http://tap4deal.a2a-tech.com/"
Browser("MainBrowser").Page("HomePage").Sync
Browser("MainBrowser").Page("HomePage").Link("Sign In").Click
Browser("MainBrowser").Page("Sign in").Link("hereForPassword").Click
Browser("MainBrowser").Page("ForgotPassword").WebEdit("email").Set Parameter("emailAdd") 
Browser("MainBrowser").Page("ForgotPassword").WebButton("Submit").Click

If Browser("MainBrowser").Page("Sign in").WebElement("SuccessPassword").Exist(2) Then
	Reporter.ReportEvent micPass, "Check password reset", "Password reset successfully"
ElseIf Not Browser("MainBrowser").Page("Sign in").WebElement("SuccessPassword").Exist(2) Then
	Reporter.ReportEvent micFail, "Check password reset", "Password reset failed"
End If