﻿'Script Name: Tappie card activate
'Title: Check client can activate the tappie card with a valid user account information
'URL: http://tap4deal.a2a-tech.com/index.php?route=account/activate
'IE 10.0.4 (KB2817183)
'
'Author: Tony Lu, <xudong.lu@yahoo.com>
'Change log: 
'      ---  Created by Tony Lu on 2013.05.14
'QA Manager: Alvin
'=======================================================================================

SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe"
wait(2)
Browser("MainBrowser").Navigate "http://tap4deal.a2a-tech.com/"
Browser("MainBrowser").Page("HomePage").Sync
Browser("MainBrowser").Page("HomePage").Link("Sign In").Click
Browser("MainBrowser").Page("Sign in").Link("hereToActivate").Click
Browser("MainBrowser").Page("TappieCardActivate").WebEdit("card_number").Set RandomNumber("InitialNumber")
Browser("MainBrowser").Page("TappieCardActivate").Link("hereForSignInWithNewTappie").Click
Browser("MainBrowser").Page("Sign in").WebEdit("email").Set Parameter("username") 
Browser("MainBrowser").Page("Sign in").WebEdit("password").Set Parameter("password") 
Browser("MainBrowser").Page("Sign in").WebButton("Login").Click
Browser("MainBrowser").Page("MyTappieCard").WebEdit("NewTappie").Set RandomNumber("RandomNumber")
Browser("MainBrowser").Page("MyTappieCard").WebEdit("confirmNewTappie").Set RandomNumber("RandomNumber")
Browser("MainBrowser").Page("MyTappieCard").WebButton("Update").Click
If Browser("MainBrowser").Page("MyTappieCard").WebElement("Success: Your Tappie Card").Exist(2) Then
	Reporter.ReportEvent micPass, "Check new tappie card", "New tappie card updated successfully"
ElseIf Not Browser("MainBrowser").Page("MyTappieCard").WebElement("Success: Your Tappie Card").Exist(2) Then
	Reporter.ReportEvent micFail, "Check new tappie card", "New tappie card updated failed"
End If