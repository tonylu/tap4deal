﻿'Script Name: client log in function test
'Title: Check client can log in with valid username and password
'URL: http://tap4deal.a2a-tech.com/index.php?route=common/home 
'IE 10.0.4 (KB2817183)
'Notes: the page title after log in is the same as home page
'   --- will suggest to modify if applicable
'Author: Tony Lu, <xudong.lu@yahoo.com>
'Change log: 
'      ---  Created by Tony Lu on 2013.05.14
'QA Manager: Alvin
'=======================================================================================


SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe"
wait(2)
Browser("MainBrowser").Navigate "http://tap4deal.a2a-tech.com/"
Browser("MainBrowser").Page("HomePage").Sync
Browser("MainBrowser").Page("HomePage").Link("Sign In").Click
Browser("MainBrowser").Page("Sign in").WebEdit("email").Set Parameter("username")
Browser("MainBrowser").Page("Sign in").WebEdit("password").Set Parameter("password")
Browser("MainBrowser").Page("Sign in").WebButton("Login").Click
If Browser("MainBrowser").Page("Sign in").Link("My Account").Exist(1) Then
	Reporter.ReportEvent micPass, "Client log in", "Client log in successfully"
ElseIf Not Browser("MainBrowser").Page("Sign in").Link("My Account").Exist(1) Then
	Reporter.ReportEvent micFail, "Client log in", "Client log in failed"
End If
Browser("MainBrowser").Page("Sign in").Link("Sign Out").Click